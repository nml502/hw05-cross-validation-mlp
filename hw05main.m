function hw05main
%HW05, main function.
%Running all the experiments takes some time.
    prob1();   %run experiments and collect results for problem1
    prob2();   %run experiments and collect results for problem2
end

function prob1
%HW05, Problem 1 (cross validation)
%Function to setup the experiments and run BP
    [ST,I] = dbstack;
    this_function = ST.name;
    %set parameters for the multi layer perceptron
    a     = 1.7159;              %transfer function = a*tanh(bx)
    b     = 2/3;
    [f,g] = make_fg(a,b);        %transfer function and its derivative
    h     = [3];                 %hidden layers (#PEs in each hidden layer)
    test_tol  = 4;               %ok to stop learning at this error percentage mishits tolerance
    train_tol = 1;               %ok to stop learning at this error percentage mishits tolerance
    mu  = 1e-2;                  %learning step
    N   = 1e2;                   %maximum learning epochs
    K   = 1;                     %batch update size
    alpha = 0.9;                 %forgetting factor for momentum update (0<= alpha <1)
    num_layers = length(h)+1;    %hidden + output layers
    record_events = 100;        %Total number of instants at which to report statistics
    error_train   = zeros(1,1+record_events);
    error_test    = zeros(1,1+record_events);
    learn_steps   = zeros(1,1+record_events);
    osuffix    = fix(clock);     %suffix for output files
    ofile      = strcat(this_function,'out',sprintf([repmat('_%d', 1, size(osuffix, 2)) '\n'],osuffix ),'.txt');
    outfile    = fopen(ofile,'wt');
    figsuffix  = sprintf([repmat('_%d', 1, size(osuffix, 2))],osuffix );
    ofig       = strcat(this_function,'fig',sprintf([repmat('_%d', 1, size(osuffix, 2)) '\n'],osuffix ),'.fig');
    fprintf('Performance characteristics will be printed in file: %s\n',ofile);
    fprintf('Performance graphs will be plotted in files with suffixes: %s\n',figsuffix);
    
    %generate training, test data, pre-process data
    [XtrainIn,DtrainIn,XtestIn,DtestIn] = generate_data_prob1();
    fold_count = 3;
    [xtrain, dtrain, xtest, dtest] = generate_fold(XtrainIn,DtrainIn,XtestIn,DtestIn,fold_count);
    ytrain = cell(1,fold_count);
    ytest  = cell(1,fold_count);
    
    %initialize figure for plotting learning history for different folds
    fig = figure;
    hold on
    title('Learning History');
    xlabel('learning step');
    ylabel('mishit percentage error');
    hold off
    col_buffer_train  = {'blue' , 'black', 'red'};
    col_buffer_test   = {'magenta' , 'cyan', 'green'};
    leg = {};

    for current_fold = 1:fold_count
        %get current fold data
        Xtrain = xtrain{current_fold};
        Dtrain = dtrain{current_fold};
        Xtest  = xtest{current_fold};
        Dtest  = dtest{current_fold}; 
      
        %pre-process data (normalize input data ranges, output data ranges etc..)
        Xtrain_mean =  mean(Xtrain(2:end,:),2);
        Xtrain_std  =  std(Xtrain(2:end,:),0,2);
        Xtest_mean  =  mean(Xtest(2:end,:),2);
        Xtest_std   =  std(Xtest(2:end,:),0,2);
        Xtrain(2:end,:) = bsxfun(@minus, Xtrain(2:end,:),Xtrain_mean);
        Xtrain(2:end,:) = bsxfun(@rdivide, Xtrain(2:end,:),Xtrain_std);
        Xtest(2:end,:)  = bsxfun(@minus, Xtest(2:end,:),Xtest_mean);
        Xtest(2:end,:)  = bsxfun(@rdivide, Xtest(2:end,:),Xtest_std);
        Dtrain(Dtrain==0) = -1;
        Dtest(Dtest==0)   = -1;
        
        [mx,nx] = size(Xtrain);
        [md,nd] = size(Dtrain);
        recall_step_size = floor(N*nx/record_events);
        
        %learn the network using xtrain, dtrain (current fold)
        fprintf(outfile,'***Starting Training for fold=%d***\n',current_fold);
        fprintf('***Starting Training for fold=%d***\n',current_fold);

        [W_trained,total_iter]  = bp_learn(Xtrain,Dtrain,h,f,g,@bp_stop,mu,N,K,alpha,@bp_recall,recall_step_size);
        
        %evaluate outputs and erros after training
        ytrain{current_fold}   = evaluate_output(W_trained,Xtrain);
        ytest{current_fold}    = evaluate_output(W_trained,Xtest);
        error_train  = error_train(1:floor(total_iter/recall_step_size));
        error_test   = error_test(1:floor(total_iter/recall_step_size));
        learn_steps  = learn_steps(1:floor(total_iter/recall_step_size));
        
        %output trained parameeters
        report_weights(outfile,W_trained);
        fprintf(outfile,'\tmishit error on training data: %f%%\n',mishit_error(W_trained,Xtrain,Dtrain));
        fprintf(outfile,'\tmishit error on test data: %f%%\n',mishit_error(W_trained,Xtest,Dtest));
        fprintf(outfile,'****Total learning steps when training stopped***\n \t%d\n',total_iter);
        
        %plot learning history
        col_train = col_buffer_train{current_fold};
        col_test  = col_buffer_test{current_fold};       
        figure(fig);
        hold on
        plot(learn_steps,error_train,'color',col_train,'markers',2,'LineWidth',2);
        plot(learn_steps,error_test,'color',col_test,'markers',2,'LineWidth',2);      
        leg{end+1} = sprintf('Training, fold=%d',current_fold);  
        leg{end+1} = sprintf('Test, fold=%d',current_fold);
        legend(leg);
        grid on;
        grid minor;
        hold off;
        saveas(gcf,sprintf('%sfig_learn_history_%s.fig',this_function,figsuffix));
    end
    
    for current_fold = 1:fold_count
        ytrain{current_fold}(ytrain{current_fold}<0) = 0;
        ytest{current_fold}(ytest{current_fold}<0)   = 0;
        dtrain{current_fold}(dtrain{current_fold}<0) = 0;
        dtest{current_fold}(dtest{current_fold}<0)   = 0;
        mul4plot = [1;2;3];
        Ytrain = 3*mean(bsxfun(@times, ytrain{current_fold},mul4plot));
        Ytest  = 3*mean(bsxfun(@times, ytest{current_fold},mul4plot));
        Dtrain = 3*mean(bsxfun(@times, dtrain{current_fold},mul4plot));
        Dtest  = 3*mean(bsxfun(@times, dtest{current_fold},mul4plot));
        title_str = sprintf('Test: Desired Vs Actual, Fold=%d',current_fold);
        fig=fig+1;
        figure(fig);
        plot(Dtest,'bs');
        hold on;
        plot(Ytest,'r*');
        title(title_str);
        xlabel('Test sample');
        ylabel('          Setosa          Versacolor          Virginica          ');
        ylim([0 4]);
        legend('Desired','Actual');
        hold off;
        saveas(gcf,sprintf('%sfig%d_test_output_%s.fig',this_function,fig,figsuffix));
    end
    
    %calculate and print confusion matrices
    cm_train = {zeros(3,3),zeros(3,3),zeros(3,3)}; %reserve space for confusion matrices
    cm_test = {zeros(3,3),zeros(3,3),zeros(3,3)};
    for current_fold=1:fold_count        
        %calculate and print confusion matrices
        [md,nd] = size(dtrain{current_fold});
        for ii = 1:nd
            true_class = find(dtrain{current_fold}(:,ii)==1);
            pred_class = find(ytrain{current_fold}(:,ii)==1);
            cm_train{current_fold}(true_class,pred_class) = cm_train{current_fold}(true_class,pred_class)+1;
        end
        [md,nd] = size(dtest{current_fold});
        for ii = 1:nd
            true_class = find(dtest{current_fold}(:,ii)==1);
            pred_class = find(ytest{current_fold}(:,ii)==1);
            cm_test{current_fold}(true_class,pred_class) = cm_test{current_fold}(true_class,pred_class)+1;
%             fprintf('cmtest, ii:%d, true_class:%d cm_test(true_class,true_class)=%d\n',ii,true_class,cm_test(true_class,true_class));
        end
        fprintf('**********\n');
        fprintf('\t\tTRAINING DATA, FOLD %d',current_fold);
        fprintf('\t\t\t\t TEST DATA, FOLD %d\n',current_fold);
        fprintf('\tP r e d i c t e d\tC l a s s \t\t P r e d i c t e d\tC l a s s\n');
        fprintf('T \t\t\t\t\t\t\t\t\t T\n');
        fprintf('r \t\t Set \t Ver \t Vir \t\t r \t\t Set \tVer \t Vir\n');
        fprintf('u \t\t\t\t\t\t\t\t\t u\n');
        fprintf('e \tSet\t %03d\t %03d\t %03d\t\t e \tSet\t %03d\t %03d\t %03d\n',cm_train{current_fold}(1,1),cm_train{current_fold}(1,2),cm_train{current_fold}(1,3),cm_test{current_fold}(1,1),cm_test{current_fold}(1,2),cm_test{current_fold}(1,3));
        fprintf('  \t\t\t\t\t\t\t\t\t  \n');
        fprintf('C  \tVer\t %03d\t %03d\t %03d\t\t e \tVer\t %03d\t %03d\t %03d\n',cm_train{current_fold}(2,1),cm_train{current_fold}(2,2),cm_train{current_fold}(2,3),cm_test{current_fold}(2,1),cm_test{current_fold}(2,2),cm_test{current_fold}(2,3));
        fprintf('l \t\t\t\t\t\t\t\t\t l\n');
        fprintf('a  \tVir\t %03d\t %03d\t %03d\t\t e \tVir\t %03d\t %03d\t %03d\n',cm_train{current_fold}(3,1),cm_train{current_fold}(3,2),cm_train{current_fold}(3,3),cm_test{current_fold}(3,1),cm_test{current_fold}(3,2),cm_test{current_fold}(3,3));
        fprintf('s \t\t\t\t\t\t\t\t\t s\n');
        fprintf('s \t\t\t\t\t\t\t\t\t s\n');
    end

    %calculate and print classification accuracies, generalization
    %performance
    accuracy = zeros(2,fold_count);
    for current_fold = 1:fold_count
        accuracy(1,current_fold) = 100*trace(cm_train{current_fold})/sum(cm_train{current_fold}(:));
        accuracy(2,current_fold) = 100*trace(cm_train{current_fold})/sum(cm_train{current_fold}(:));
    end
    accuracy_avg = mean(accuracy,2);
    accuracy_std = std(accuracy,0,2);

    fprintf('**********\n');
    fprintf('\tCLASSIFICATION ACCURACY\n\n');
    fprintf('\t\t\t\t Fold=1 \t Fold=2 \t Fold=3 \t \tAverage\t \tStandardDeviation\n');
    fprintf('\tTraining\t \t%03d\t  \t\t%03d\t \t\t%03d\t\t \t%g\t\t\t \t%g\n',accuracy(1,1),accuracy(1,2),accuracy(1,3),accuracy_avg(1),accuracy_std(1));
    fprintf('\tTest    \t \t%03d\t  \t\t%03d\t \t\t%03d\t\t \t%g\t\t\t \t%g\n',accuracy(2,1),accuracy(2,2),accuracy(2,3),accuracy_avg(2),accuracy_std(2));    
    
    function bp_recall(iter,W)
    %nested function to recall and record performance characteristics
    %during learning    %Input
    %   iter : current iteration counter
    %   W     : cell array containing weights of multi layer perceptron
    %Output
    %   No return value.
        training_error = mishit_error(W,Xtrain,Dtrain);
        test_error     = mishit_error(W,Xtest,Dtest);
        if(iter == 0)
            %Report network parameters
            fprintf(outfile,'----------BP LEARN STARTED----------\n');
            fprintf(outfile,'***Initial weights***\n');
            report_weights(outfile,W);
            fprintf(outfile,'***Learning rate***\n');
            fprintf(outfile,'\t%g\n', mu);
            fprintf(outfile,'***Momentum Update (forgetting factor)***\n');
            fprintf(outfile,'\t%g\n', alpha);
            fprintf(outfile,'***Stopping Criteria = max. learn steps OR mishit (percentage) train and test error < tolerance***\n');
            fprintf(outfile,'\tMax learning steps\n');
            fprintf(outfile,'\t%d\n',N*nx);
            fprintf(outfile,'\tTraiing Error Tolerance\n');
            fprintf(outfile,'\t%g%%\n',train_tol);
            fprintf(outfile,'\tTest Error Tolerance\n');
            fprintf(outfile,'\t%g%%\n',test_tol);
            fprintf(outfile,'***Initial mishit Error for training ***\n');
            fprintf(outfile,'\t%f%%\n',training_error);
            fprintf(outfile,'***Initial mishit Error for test ***\n');
            fprintf(outfile,'\t%f%%\n',test_error);
            fprintf(outfile,'***Performance indicators while running BP algorithm***\n');
        else
            fprintf(outfile,'\titer: %d, training mishit error:%f%%, test mishit error:%f%%\n',iter,training_error,test_error);
            fprintf('\titer: %d, training mishit error:%f%%, test mishit error:%f%%\n',iter,training_error,test_error);
        end
        learn_steps(1 + iter/recall_step_size) = iter;
        error_train(1 + iter/recall_step_size) = training_error;   %record training and test errors
        error_test(1 + iter/recall_step_size)  = test_error;
    end

    function report_weights(ofile,W)
    %function to print out current set of weights
    %Input
    %   ofile : output file (weights will be printed to this file)
    %           It is assumed to be open.
    %   W     : cell array containing weights of multi layer perceptron
    %Output
    %   No return value.
         fprintf(ofile,'\tW%d\n',1);
         fprintf(ofile,[repmat('\t%+8.6f ',1,mx) '\n'], W{1}); 
         for layer = 2:numel(W)
            fprintf(ofile,'\tW%d\n',layer);
            fprintf(ofile,[repmat('\t%+8.6f ',1,h(layer-1)+1) '\n'], W{layer});             
         end
    end
    
    function error = mishit_error(W,X,D)
    %nested function to evaluate mishit error given weights W, input X
    %and desired output D.
        [mD, nD] = size(D);
        error = evaluate_output(W,X) - D;
        error(error~=0) = 1;
        error = mean(error);
        error(error~=0) = 1;
        mishits = sum(error);
        error = (mishits/nD)*100;
    end

    function stop = bp_stop(W)
    %nested function for bp error handle predicate
        if(mishit_error(W,Xtrain,Dtrain) <= train_tol && mishit_error(W,Xtest,Dtest)<= test_tol) 
            stop = true;
        else
            stop = false;
        end
    end

    function Y = evaluate_output(W,X)
    %nested function to evaluate ouput given weights W, input X
        Y = f(W{1}*X);                     %input layer
        [row_x,num_x] = size(X);
        for i = 2:num_layers               %hidden and output layers
            Y = f(W{i}*[ones(1,num_x);Y]);        
        end
        [maxy,ind] = max(Y);
        Y = Y*0 -1;
        for i = 1:length(ind)
            Y(ind(i),i) = 1;
        end
    end

    fclose('all');
    fprintf('**********\n');
    fprintf('\tFor more learn statistics see text file:\t%s\n',ofile);
    display('Finished');
end

function prob2
%HW05, Problem 2 (train MLP for equalization of memoryless channel)
%Function to setup the experiments and run BP
    [ST,I] = dbstack;
    this_function = ST.name;
    
    %generate training, test data, pre-process data
    npoints          = 300;
    [ntrain,Xtrain,Dtrain,ntest,Xtest,Dtest] = generate_data_prob2(npoints);    
    Xtrain_mean      = mean(Xtrain(2:end,:),2);
    Dtrain_mean      = mean(Dtrain);
    Xtrain_std       = std(Xtrain(2:end,:),0,2);
    Xtest_mean       = mean(Xtest(2:end,:),2);
    Dtest_mean       = mean(Dtest);
    Xtest_std        = std(Xtest(2:end,:),0,2);
    output_scale     = max(max(Dtest),max(Dtrain));
    Xtrain(2:end,:)  = bsxfun(@minus, Xtrain(2:end,:),Xtrain_mean);
    Xtrain(2:end,:)  = bsxfun(@rdivide, Xtrain(2:end,:),Xtrain_std);
    Dtrain           = bsxfun(@minus, Dtrain, Dtrain_mean);
    Dtrain           = bsxfun(@rdivide, Dtrain, output_scale);
    Xtest(2:end,:)   = bsxfun(@minus, Xtest(2:end,:),Xtest_mean);
    Xtest(2:end,:)   = bsxfun(@rdivide, Xtest(2:end,:),Xtest_std);
    Dtest            = bsxfun(@minus, Dtest, Dtest_mean);
    Dtest            = bsxfun(@rdivide, Dtest, output_scale);
    
    [mx,nx] = size(Xtrain);      %data dimensions, #data points
    [md,nd] = size(Dtrain);
    a     = 1.7159;              %transfer function = a*tanh(bx)
    b     = 2/3;
    [f,g] = make_fg(a,b);        %get transfer function and its derivative
    h   = [80];                  %hidden layers (#PEs in each hidden layer)
    train_tol = 1e-2;            %ok to stop learning at this training error tolerance (RMS)
    test_tol = 1e-2;             %ok to stop learning at this test error tolerance (RMS)
    mu  = 1e-2;                  %learning step
    N   = 1e3;                   %maximum learning epochs
    K   = 2e1;                   %batch update size
    alpha = 0.5;                 %forgetting factor for momentum update (0<= alpha <1)
    num_layers = length(h)+1;    %hidden + output layers
    record_events = 400;         %Total number of instants at which to report statistics while learning
    recall_step_size = floor(N*nx/record_events);
    error_train      = zeros(1,1+record_events);
    error_test       = zeros(1,1+record_events);
    learn_steps      = zeros(1,1+record_events);
    
    %setup output reporting files, figures
    osuffix  = fix(clock);        %suffix for output files
    ofile    = strcat(this_function,'out',sprintf([repmat('_%d', 1, size(osuffix, 2)) '\n'],osuffix ),'.txt');
    outfile  = fopen(ofile,'wt');
    figsuffx = sprintf([repmat('_%d', 1, size(osuffix, 2))],osuffix );
    fprintf('Performance characteristics will be printed in file: %s\n',ofile);
    fprintf('Performance graphs will be plotted in file with suffix: %s\n',figsuffix);

    
    function bp_recall(iter,W)  
    %nested function to recall and record performance characteristics
    %during learning    %Input
    %   iter : current iteration counter
    %   W     : cell array containing weights of multi layer perceptron
    %Output
    %   No return value.
        training_error = rms_error(W,Xtrain,Dtrain);
        test_error     = rms_error(W,Xtest,Dtest);
        if(iter == 0)
            %Report network parameters
            fprintf(outfile,'----------BP LEARN STARTED----------\n');
            fprintf(outfile,'***Initial weights***\n');
            report_weights(outfile,W);
            fprintf(outfile,'***Learning rate***\n');
            fprintf(outfile,'\t%g\n', mu);
            fprintf(outfile,'***Momentum Update (forgetting factor)***\n');
            fprintf(outfile,'\t%g\n', alpha);
            fprintf(outfile,'***Stopping Criteria = max. learn steps OR rms train and test error < tolerance***\n');
            fprintf(outfile,'\tMax learning steps\n');
            fprintf(outfile,'\t%d\n',N*nx);
            fprintf(outfile,'\tTraiing Error Tolerance\n');
            fprintf(outfile,'\t%g%%\n',train_tol);
            fprintf(outfile,'\tTest Error Tolerance\n');
            fprintf(outfile,'\t%g%%\n',test_tol);
            fprintf(outfile,'***Initial RMS Error on training data ***\n');
            fprintf(outfile,'\t%f%%\n',training_error);
            fprintf(outfile,'***Initial RMS Error on test data ***\n');
            fprintf(outfile,'\t%f%%\n',test_error);
            fprintf(outfile,'***Performance indicators while running BP algorithm***\n');
        else
            fprintf(outfile,'\titer: %d, training rms error:%f, test rms error:%f\n',iter,training_error,test_error);
            fprintf('\titer: %d, training rms error:%f, test rms error:%f\n',iter,training_error,test_error);
        end
        learn_steps(1 + iter/recall_step_size) = iter;
        error_train(1 + iter/recall_step_size) = training_error;  %record training and test errors
        error_test(1 + iter/recall_step_size)  = test_error;
    end

    function report_weights(ofile,W)
    %function to print out current set of weights
    %Input
    %   ofile : output file (weights will be printed to this file)
    %           It is assumed to be open.
    %   W     : cell array containing weights of multi layer perceptron
    %Output
    %   No return value.
         fprintf(ofile,'\tW%d\n',1);
         fprintf(ofile,[repmat('\t%+8.6f ',1,mx) '\n'], W{1}); 
         for layer = 2:numel(W)
            fprintf(ofile,'\tW%d\n',layer);
            fprintf(ofile,[repmat('\t%+8.6f ',1,h(layer-1)+1) '\n'], W{layer});             
         end
    end
    

    function error = rms_error(W,X,D)
    %nested function to evaluate rms error given weights W, input X
    %and desired output D.
        error = rms(output_scale*evaluate_output(W,X) - output_scale*D);
    end

    function stop = bp_stop(W)
    %nested function for bp error handle predicate
        if(rms_error(W,Xtrain,Dtrain) <= train_tol && rms_error(W,Xtest,Dtest)<= test_tol) 
            stop = true;
        else
            stop = false;
        end
    end

    function Y = evaluate_output(W,X)
    %nested function to evaluate ouput given weights W, input X
        Y = f(W{1}*X);                     %input layer
        [row_x,num_x] = size(X);
        for i = 2:num_layers               %hidden and output layers
            Y = f(W{i}*[ones(1,num_x);Y]);        
        end
    end

    dtrain      = output_scale*Dtrain + Dtrain_mean;
    dtest       = output_scale*Dtest  + Dtest_mean;
    Ytrain      = dtrain*0;
    Ytest       = dtest*0;
    minx        = min([ntrain, ntest]);
    maxx        = max([ntrain, ntest]);
    miny        = min([dtrain, dtest]);
    maxy        = max([dtrain, dtest]);
    axisXY      = [minx maxx miny-1 maxy+1];
    col_buffer  = {'blue' , 'red', 'black',  'magenta', 'yellow', 'cyan'};
    
    function init_plot(fig)
        figure(fig)
        subplot(2,1,1);   %plot Input, Trained Output, Test Output
        hold on;
        plot(ntrain,dtrain,'g--*','LineWidth',2,'markers',3);
        axis(axisXY);
        title('Training data : Desired Vs Actual output');
        xlabel('n'); ylabel('2sin(2*pi*n/20)');
        leg1{end+1} = 'Desired';
        legend(leg1);
        grid on;
        hold off;
        
        subplot(2,1,2);
        hold on
        plot(ntest,dtest,'g--s','LineWidth',3,'markers',2);
        axis(axisXY);
        title('Test data : Desired Vs Actual output');
        xlabel('n'); ylabel('2sin(2*pi*n/20)');
        leg2{end+1} = 'Desired';
        legend(leg2);
        grid on;
        hold off;
        
        figure(fig+1)
        subplot(2,1,1);
        hold on
        title('Learning History : Training');
        xlabel('Learning Step');
        ylabel('RMS error');
        axis([1 6e5 0 1])
        hold off;
        
        subplot(2,1,2);
        hold on;
        title('Learning History : Test');
        xlabel('Learning Step');
        ylabel('RMS error');
        axis([1 6e5 0 1])
        hold off      
    end

    function print_plot_results(ofile,heading,col,leg_suffix,fig,figfile,figsuffx)
        fprintf(ofile,heading);
        report_weights(ofile,W_trained);
        fprintf(ofile,'\trms error on training data: %f\n',error_train(end));
        fprintf(ofile,'\trms error on test data: %f\n',error_test(end));
        fprintf(ofile,'****Total learning steps when training stopped***\n \t%d\n',total_iter);
    
        %After training, plot desired outputs vs network outputs on training and test data
        figure(fig)
        subplot(2,1,1);   %plot Input, Trained Output, Test Output
        hold on;
        plot(ntrain,Ytrain,'color',col,'markers',2,'LineWidth',2);
        leg1{end+1} = sprintf('%s',leg_suffix);
        legend(leg1);
        hold off;
        subplot(2,1,2);
        hold on;
        plot(ntest,Ytest,'color',col,'markers',2,'LineWidth',2);
        leg2{end+1} = sprintf('%s',leg_suffix);  
        legend(leg2);
        hold off;
        saveas(gcf,sprintf('%sfig_data_compare_%s_%s.fig',this_function,figfile,figsuffx));

        figure(fig+1)
        subplot(2,1,1);
        hold on;
        plot(learn_steps,error_train,'color',col,'markers',2,'LineWidth',2);
        leg3{end+1} = sprintf('%s',leg_suffix); 
        legend(leg3);
        hold off;
        
        subplot(2,1,2);  
        hold on;
        plot(learn_steps,error_test,'color',col,'markers',2,'LineWidth',2);
        leg4{end+1} = sprintf('%s',leg_suffix);  
        legend(leg4);
        hold off;
        saveas(gcf,sprintf('%sfig_error_history_%s_%s.fig',this_function,figfile,figsuffx));
    end

    fig    = figure;
    leg1   = {}; leg2 = {}; leg3 = {}; leg4 = {};
    cindex = 1;
    init_plot(fig);

    for mu = [1e-2 5e-2 5e-3 5e-4]
        %train network using belief propagation
        fprintf('Starting training for mu = %g...\n',mu);
        [W_trained,total_iter]  = bp_learn(Xtrain,Dtrain,h,f,g,@bp_stop,mu,N,K,alpha,@bp_recall,recall_step_size);
    
        %evaluate outputs and erros after training
        Ytrain = output_scale*evaluate_output(W_trained,Xtrain) + Dtrain_mean;
        Ytest    = output_scale*evaluate_output(W_trained,Xtest) + Dtest_mean;    
        error_train  = error_train(1:floor(total_iter/recall_step_size));
        error_test   = error_test(1:floor(total_iter/recall_step_size));
        learn_steps = learn_steps(1:floor(total_iter/recall_step_size));        
        heading    = sprintf('***After training with mu = %g***\n',mu);
        leg_suffix = sprintf('mu = %g',mu);
        print_plot_results(outfile,heading,col_buffer{cindex},leg_suffix,fig,'learn_rate',figsuffx);
        cindex     = cindex+1;
    end
    
    fig = fig+2;
    leg1 = {}; leg2 = {}; leg3 = {}; leg4 = {};
    cindex = 1;
    mu = 1e-2;
    init_plot(fig);
    
     for K = [1 1e1 2e1 2e2]
        %train network using belief propagation
        fprintf('Starting training for batch size,K = %g...\n',K);
        [W_trained,total_iter]  = bp_learn(Xtrain,Dtrain,h,f,g,@bp_stop,mu,N,K,alpha,@bp_recall,recall_step_size);
    
        %evaluate outputs and erros after training
        Ytrain = output_scale*evaluate_output(W_trained,Xtrain) + Dtrain_mean;
        Ytest    = output_scale*evaluate_output(W_trained,Xtest) + Dtest_mean;    
        error_train  = error_train(1:floor(total_iter/recall_step_size));
        error_test   = error_test(1:floor(total_iter/recall_step_size));
        learn_steps = learn_steps(1:floor(total_iter/recall_step_size));
        
        heading    = sprintf('***After training with batch size, K = %g***\n',K);
        leg_suffix = sprintf('K = %g',K);
        print_plot_results(outfile,heading,col_buffer{cindex},leg_suffix,fig,'batch_size',figsuffx);
        cindex     = cindex+1;
     end
     
    fig  = fig+2;
    leg1 = {}; leg2 = {}; leg3 = {}; leg4 = {};
    cindex = 1;
    K = 2e1;
    init_plot(fig);
    
    for alpha = [0.10 0.25 0.5 0.75 0.90]
        %train network using belief propagation
        fprintf('Starting training for momentum, alpha = %g...\n',K);
        [W_trained,total_iter]  = bp_learn(Xtrain,Dtrain,h,f,g,@bp_stop,mu,N,K,alpha,@bp_recall,recall_step_size);
    
        %evaluate outputs and erros after training
        Ytrain = output_scale*evaluate_output(W_trained,Xtrain) + Dtrain_mean;
        Ytest    = output_scale*evaluate_output(W_trained,Xtest) + Dtest_mean;    
        error_train  = error_train(1:floor(total_iter/recall_step_size));
        error_test   = error_test(1:floor(total_iter/recall_step_size));
        learn_steps = learn_steps(1:floor(total_iter/recall_step_size));
        
        heading    = sprintf('***After training with momentum, alpha = %g***\n',K);
        leg_suffix = sprintf('alpha = %g',alpha);
        print_plot_results(outfile,heading,col_buffer{cindex},leg_suffix,fig,'momentum_alpha',figsuffx);
        cindex     = cindex+1;
    end
    
    fig  = fig+2;
    leg1 = {}; leg2 = {}; leg3 = {}; leg4 = {};
    cindex = 1;
    alpha = 0.5;
    init_plot(fig);
    
    ch = {[20] , [50], [80], [100], [20 10], [30,20]};
    for hi = 1:numel(ch)
        h = ch{hi};
        lenh = length(h);
        num_layers = length(h)+1;    %hidden + output layers
        %train network using belief propagation
        hstr = sprintf([ '[' repmat('%d ',1,lenh) ']'], h); 
        fprintf('Starting training for hidden layers,h = %s...\n',hstr );
    
        [W_trained,total_iter]  = bp_learn(Xtrain,Dtrain,h,f,g,@bp_stop,mu,N,K,alpha,@bp_recall,recall_step_size);
    
        %evaluate outputs and erros after training
        Ytrain = output_scale*evaluate_output(W_trained,Xtrain) + Dtrain_mean;
        Ytest    = output_scale*evaluate_output(W_trained,Xtest) + Dtest_mean;    
        error_train  = error_train(1:floor(total_iter/recall_step_size));
        error_test   = error_test(1:floor(total_iter/recall_step_size));
        learn_steps = learn_steps(1:floor(total_iter/recall_step_size));
        
        heading    = sprintf('***After training with hidden layers, h = %s***\n',hstr);
        leg_suffix = sprintf('h = %s',hstr);
        print_plot_results(outfile,heading,col_buffer{cindex},leg_suffix,fig,'hidden_layers',figsuffx);
        cindex     = cindex+1;
    end
    
    %Best results
    h = [30 20];
    lenh = length(h);
    num_layers = length(h)+1;    %hidden + output layers
    mu = 0.01;
    K  = 1;
    alpha = 0.9;
    hstr = sprintf([ '[' repmat('%d ',1,lenh) ']'], h); 
    fig  = fig+2;
    leg1 = {}; leg2 = {}; leg3 = {}; leg4 = {};
    cindex = 1;
    init_plot(fig);
    
    %train network using belief propagation
    fprintf('Best Parameters: starting training for hidden layers,h = %s, mu=%g, K=%d, alpha=%g...\n',hstr, mu , K, alpha );
    [W_trained,total_iter]  = bp_learn(Xtrain,Dtrain,h,f,g,@bp_stop,mu,N,K,alpha,@bp_recall,recall_step_size);
    
    %evaluate outputs and erros after training
    Ytrain = output_scale*evaluate_output(W_trained,Xtrain) + Dtrain_mean;
    Ytest    = output_scale*evaluate_output(W_trained,Xtest) + Dtest_mean;    
    error_train  = error_train(1:floor(total_iter/recall_step_size));
    error_test   = error_test(1:floor(total_iter/recall_step_size));
    learn_steps = learn_steps(1:floor(total_iter/recall_step_size));        
    heading    = sprintf('***After training with hidden layers, h = %s, mu=%g, K=%d, alpha=%g...\n',hstr, mu , K, alpha);
    leg_suffix = sprintf('h=%s,mu=%g,K=%d,alpha=%g',hstr, mu , K, alpha);
    print_plot_results(outfile,heading,col_buffer{cindex},leg_suffix,fig,'best_parameters',figsuffx);
    
    
    %Document results for problem2.2
    npoints = 300;
    [X1,D1,X2,D2] = generate_data_prob2_2(npoints);
    N1 = 1:length(X1);
    N2 = 1:length(X2);
    X1_mean      = mean(X1(2:end,:),2);
    D1_mean      = mean(D1);
    X1_std       = std(X1(2:end,:),0,2);
    X2_mean      = mean(X2(2:end,:),2);
    D2_mean      = mean(D2);
    X2_std       = std(X2(2:end,:),0,2);
    output_scale1= max(D1);
    output_scale2= max(D2);
    X1(2:end,:)  = bsxfun(@minus, X1(2:end,:),X1_mean);
    X1(2:end,:)  = bsxfun(@rdivide, X1(2:end,:),X1_std);
    X2(2:end,:)  = bsxfun(@minus, X2(2:end,:),X2_mean);
    X2(2:end,:)  = bsxfun(@rdivide, X2(2:end,:),X2_std);
    Y1 = output_scale1*evaluate_output(W_trained,X1) + D1_mean;
    Y2 = output_scale2*evaluate_output(W_trained,X2) + D2_mean;
    
    fprintf(outfile,'***Test on data s1(n) = 0.8sin(2*pi*n/20) + 0.25cos(2*pi*n/25)***\n');
    fprintf(outfile,'\trms error : %f\n',rms(output_scale1*evaluate_output(W_trained,X1) - D1));
    fprintf(outfile,'***Test on data s2(n) = normal distribution (zero mean and unit variance)***\n');
    fprintf(outfile,'\trms error : %f\n',rms(output_scale2*evaluate_output(W_trained,X2) - D2));  
    fprintf('***Test on data s1(n) = 0.8sin(2*pi*n/20) + 0.25cos(2*pi*n/25)***\n');
    fprintf('\trms error : %f\n',rms(output_scale1*evaluate_output(W_trained,X1) - D1));
    fprintf('***Test on data s2(n) = normal distribution (zero mean and unit variance)***\n');
    fprintf('\trms error : %f\n',rms(output_scale2*evaluate_output(W_trained,X2) - D2));  
    
    figure(fig)
    subplot(2,1,1);  
    plot(N1,D1,'g--*',N1,Y1,'r--s','LineWidth',2,'markers',3);
    axis([1 npoints -2 2]);
    title('Test on s1(n) = 0.8sin(2*pi*n/20) + 0.25cos(2*pi*n/25): Desired Vs Actual output');
    xlabel('n'); ylabel('0.8sin(2*pi*n/10) + 0.25cos(2*pi*n/25)');
    legend('Desired','Actual');
    grid on;
        
    subplot(2,1,2);
    hist([D2' , Y2'],10);
  %  plot(N2,D2,'g--*',N2,Y2,'r--s','LineWidth',2,'markers',3);
  %  axis([1 npoints min(D2) max(D2)]);
    title(' Test on Normal distribution(mean=0, Variance=1): Desired Vs Actual output');
 %   xlabel('n'); ylabel('N(0,1):standard normal');
    legend('Desired','Actual');
    grid on;
    saveas(gcf,sprintf('%sfig_data_compare_part2_%s.fig',this_function,figsuffx));
   
    fclose('all');
    display('Finished');
end


function [W,iter] = bp_learn(X,D,h,f,g,stop_predicate,mu,N,K,alpha,bp_report,report_step)
%Run batch update belief propagation algorithm
%input
%   X : Input training data matrix. Columns of this matrix correspond to 
%       input vectors. Bias terms should be included in X (all these are 1)
%   D : Ouput training data matrix. Columns of this matrix correspond to
%       desired outputs for the respective inputs.
%   h : vector representing network topology. ith element of this vector
%       denotes number of processing elements in ith hidden layer
%   f : transfer function of the processing elements. This must be a 
%       function handle with one input parameter
%   g : derivative of the transfer function 'f'. This also must be a
%       function handle with one input parameter.
%   stop_predicate: predicate (function handle) to indicate if BP stops for 
%                   current set of weights 'W' before max iterations. 
%                   The max iterations are determined using 'N' (see below)
%                   and number of training patters, that is matrix X(see above).
%   mu: learning rate for BP algorithm.
% alpha:Forgetting factor for momentum update (between 0 and 1)
%   N : maximum learning epochs for the algorithm to terminate
%       epoch = number of training samples, size of matrix X (see above).
%   K : batch update step size
%   bp_report : callback function for reporting performace characteristics.
%            It must be a function handle with two parameters, 'iter' which
%            is current iteration and 'W' which is current set of weights.
%   report_step : call the 'report' callback every 'report_step'
%                iteratioins.
%output
%   W : A cell array represnting the weights of the network in each layer.
%       W{i} contains the weigth matrix for ith layer. 
    [mx,nx] = size(X);
    [md,nd] = size(D);
    num_layers = length(h)+1;   % hidden layers + output layer
      
    if (nx ~= nd)
        display('ERROR: bp : number of training inputs and outputs not same');
        return;
    end
      
    % Reserve memory for weights 'W', activation levels 'Net', outputs Y,
    % Delta (errros) at each layer.
    Y     = cell(1,num_layers);
    Net   = cell(1,num_layers);
    W     = cell(1,num_layers);
    G     = cell(1,num_layers);
    Delta = cell(1,num_layers);
    deltaW= cell(1,num_layers);
    previous_deltaW = cell(1,num_layers);
       
    for layer = 1:length(h)
        Y{layer}   = zeros(h(layer),1);
        Net{layer} = zeros(h(layer),1);
        G{layer}   = zeros(h(layer),h(layer));
    end
    Y{num_layers}     = zeros(md,1);
    Net{num_layers}   = zeros(md,1);
    G{num_layers}     = zeros(md,md);
    Delta{num_layers} = zeros(md,1);

    %randomize initial weights
%     W{1} = sqrt(1/mx)*2*(rand(h(1),mx,'double') - 0.5);
    W{1} = 0.2*(rand(h(1),mx,'double') - 0.5);
    deltaW{1} = zeros(h(1),mx);
     previous_deltaW{1} = zeros(h(1),mx);
    for layer = 2:length(h)
%       W{layer} = sqrt(1/(h(layer-1)+1))*2*(rand(h(layer),h(layer-1)+1,'double') - 0.5);
       W{layer} = 0.2*(rand(h(layer),h(layer-1)+1,'double') - 0.5);
       deltaW{layer} = zeros(h(layer),h(layer-1)+1);
       previous_deltaW{layer} = zeros(h(layer),h(layer-1)+1);
    end
%    W{num_layers} = sqrt(1/(h(end)+1))*2*(rand(md,h(end)+1,'double') - 0.5);
    W{num_layers} = 0.2*(rand(md,h(end)+1,'double') - 0.5);
    deltaW{num_layers} = zeros(md,h(end)+1);
    previous_deltaW{num_layers} = zeros(md,h(end)+1);

    
    for layer = length(h):-1:1
        [rG,cG] = size(G{layer});
        [rW,cW] = size(W{layer+1});
        [rD,cD] = size(Delta{layer+1});
        Delta{layer} = zeros(rG,cD);
    end

    %nested function to do feedforward for one learning step
    function feedforward(k)
        Net{1} = W{1}*X(:,k);                 %input layer
        Y{1}   = f(Net{1});
        
        for i = 2:num_layers                  %hidden and output layers
            Net{i} = W{i}*[1;Y{i-1}];
            Y{i} = f(Net{i});        
        end            
    end
    
    %nested function to feedback errors for one learning step
    function feedback(k)
        G{num_layers}(1:(md+1):end)= g(Net{num_layers});          %output layer
        Delta{num_layers} = G{num_layers}*(D(:,k)-Y{num_layers});
        
        for i = num_layers-1:-1:1                                 %hidden layers
            G{i}(1:h(i)+1:end) = g(Net{i});
            Delta{i} = G{i}*W{i+1}(:,2:end)'*Delta{i+1};
        end
    end
    
    %nested function to evaluate change in weights for all the layers
    function update_delta_weights(k)
        deltaW{1} = deltaW{1} + mu*Delta{1}*X(:,k)';   %input layer
        
        for i = 2:num_layers                           %remaining layers
            deltaW{i} = deltaW{i} + mu*Delta{i}*[1;Y{i-1}]';      
        end            
    end

    %nested function to reset weight deltas to zero
    function reset_delta_weights
        for i=1:num_layers
            deltaW{i} = deltaW{i}*0;
        end
    end

    %update weights : that is, do 'W + deltaW' (includes momentum update)
    function update_weights
        for i = 1:num_layers
            W{i} = W{i} + deltaW{i} + alpha* previous_deltaW{i};
            previous_deltaW{i} = deltaW{i};
        end
    end
    
    iter = 0;             %total iterations counter
    batch_iter = 0;       %counter for iteration  within a batch
    bp_report(iter,W);       %make initial report before starting BP
    
    % Backpropagation algorithm for training the network
    for epoch = 1:N
        for p = randperm(nx)     
            feedforward(p);              %feedforward
            feedback(p);                 %feedback errors
            update_delta_weights(p);     %evaluate change in weights (weight deltas)
            batch_iter = batch_iter + 1; %increment iteration counters
            iter = iter+1;
            
            if(mod(batch_iter,K) == 0)   %update weights after batch size = 'K' iterations
                update_weights();
                reset_delta_weights();
                batch_iter = 0;
            end
            
            if(mod(iter,report_step) == 0)
                bp_report(iter,W);         %perforamce record function callback
            end            
        end
        
        if(stop_predicate(W))           %stop learning if stopping criteria true    
            break;
        end
    end
end

function [Xtrain, Dtrain, Xtest, Dtest] = generate_data_prob1
%Generate training and test data for homework5 problem1 (iris dataset)
%input
%   no input (however, the iris data ascii files must be present in same
%   directory as this m file.
%output
%   [Xtrain,Dtrain] = input and output for training
%   [Xtest,Dtest]   = input and output for testing
ftrain = fopen('iris-train.txt','rt');
ftest  = fopen('iris-test.txt','rt');

persistent train_data;
persistent test_data;

if(isempty(train_data))
    display('reading iris training dataset from file iris-train.txt for Homework5, problem1');
    train_data = textscan(ftrain,'%f%f%f%f\n&%f%f%f\n','HeaderLines',8);
end
if(isempty(test_data))
    display('reading iris test dataset from file iris-test.txt for Homework5, problem1');
    test_data = textscan(ftest,'%f%f%f%f\n&%f%f%f\n','HeaderLines',8);
end

persistent xtrain;
persistent dtrain;
persistent xtest;
persistent dtest;

if(isempty(xtrain) || isempty(dtrain))
    display('generating iris training dataset for Homework5, problem1');
    xtrain = [0*train_data{1}+1, train_data{1}, train_data{2}, train_data{3}, train_data{4}]';
    dtrain = [train_data{5}, train_data{6}, train_data{7}]';
end
if(isempty(xtest) || isempty(dtest))
    display('generating iris test dataset for Homework5, problem1');
    xtest = [0*test_data{1}+1, test_data{1}, test_data{2}, test_data{3}, test_data{4}]';
    dtest = [test_data{5}, test_data{6}, test_data{7}]';
end

Xtrain = xtrain;
Dtrain = dtrain;
Xtest  = xtest;
Dtest  = dtest;

end

function [Ntrain,Xtrain,Dtrain,Ntest,Xtest,Dtest] = generate_data_prob2(npoints)
%Generate training and test data for homework5 problem2 s(n) = 2*sin(2*pi*n/20)
%input
%   npints = number of data points
%output
%   [Xtrain,Dtrain] = input and output for training
%   [Xtest,Dtest]   = input and output for testing
    persistent n;
    persistent s;
    persistent z;
    A = 1;
    B = 0.2;
    
    
    if(isempty(n) || isempty(s) || isempty(z))
        display('generating sample points for Homework5, problem2')
        n = randperm(npoints);
        s = 2*sin(2*pi*n/20);
        z = A*s + B*s.^2;
    end
    
    persistent ntrain;
    persistent xtrain;
    persistent dtrain;
    persistent xtest;
    persistent dtest;
    persistent ntest;

    if(isempty(xtrain) || isempty(dtrain) || isempty(ntrain))
        display('generating training data')
        ntrain = n(1:floor(2*npoints/3));
        xtrain = z(ntrain);
        dtrain = s(ntrain);
        [ntrain,sind] = sort(ntrain);
        xtrain = [ones(1,length(ntrain));xtrain(sind)];
        dtrain = dtrain(sind);
    end
    
    if(isempty(xtest) && isempty(dtest) || isempty(ntest))
        display('generating test data')
        ntest  = n(floor(2*npoints/3)+1:npoints);
        xtest  = z(ntest);
        dtest  = s(ntest);
        [ntest,sind] = sort(ntest);
        xtest  = [ones(1,length(ntest));xtest(sind)];
        dtest = dtest(sind);
    end
    
    Ntrain = ntrain;
    Ntest  = ntest;
    Xtrain = xtrain;
    Xtest  = xtest;
    Dtrain = dtrain;
    Dtest  = dtest;
end

function [X1,D1,X2,D2] = generate_data_prob2_2(npoints)
%Generate training and test data for homework5 problem2.2 s(n) = 2*sin(2*pi*n/20)
%input
%   npints = number of data points
%output
%   [X1,D1] =   input and desired output for s1(n) = 0.8sin(2*pi*n/10) +
%   0.25*cos(2*pi*n/25)
%   [X2,D2]   = input and desired output for testing s2(n) = N(0,1) normal
%   distribution
    persistent n;
    persistent s1;
    persistent z1;
    persistent s2;
    persistent z2;
    A = 1;
    B = 0.2;
        
    if(isempty(n) || isempty(s1) || isempty(z1) || isempty(s2) || isempty(z2) )
        display('generating sample points for Homework5, problem2.2')
        n  = 1:npoints;
        s1 = 0.8*sin(2*pi*n/10) + 0.25*cos(2*pi*n/25);
        z1 = A*s1 + B*s1.^2;
        s2 = sort(normrnd(0,1,1,npoints));
        z2 = A*s2 + B*s2.^2;
    end
    
    X1 = [ones(1,npoints);z1];
    D1 = s1;
    X2 = [ones(1,npoints);z2];
    D2 = s2;
end

function [Xtrain, Dtrain, Xtest, Dtest] = generate_fold(XtrainIn,DtrainIn,XtestIn,DtestIn,FoldCount)
%% HEADER
% Modified a little from the original
% Author: Patrick O'Driscoll
% Created: 2014/02/03
% Updated: 2015/02/18 - provided better comments and augmented header
% Description:
% Example of how to generate cross validation, and extract a single fold.

    %% Generate the folds
    % collect the total size of the dataset
    [mxTrain, nxTrain] = size(XtrainIn);
    [mxTest, nxTest] = size(XtestIn);
    InSize = nxTrain + nxTest;

    % combine the test and training data and permute them
    Perm    = randsample(1:InSize,InSize);
    InPerm  = [XtrainIn, XtestIn];
    OutPerm = [DtrainIn, DtestIn];
    InPerm = InPerm(:,Perm);
    OutPerm = OutPerm(:,Perm);

    % print warning if this will not fold evenly
    if mod(InSize,FoldCount) ~= 0
        error('WARNING: generate_fold: This will not fold evenly');
    end

    % save memory for the following loop to save computation time
    FoldIndex = zeros(FoldCount,InSize);

    % set the index of the test and training data
    for ii = 0:FoldCount-1
        temp = zeros(1,InSize);
        temp(1,ii*(1/FoldCount*InSize)+1:(ii+1)*(1/FoldCount*InSize)) = 1;
        FoldIndex(ii+1,:) = temp;
    end
    % 0 - training data
    % 1 - test data

    Xtrain = cell(1,FoldCount);
    Dtrain = cell(1,FoldCount);
    Xtest  = cell(1,FoldCount);
    Dtest  = cell(1,FoldCount);  
    %% extract the data from the specified fold
    for CurrentFold = 1:FoldCount
        Xtrain{CurrentFold} = InPerm(:,find(FoldIndex(CurrentFold,:)==0));
        Xtest{CurrentFold}  = InPerm(:,find(FoldIndex(CurrentFold,:)==1));

        Dtrain{CurrentFold} = OutPerm(:,find(FoldIndex(CurrentFold,:)==0));
        Dtest{CurrentFold}  = OutPerm(:,find(FoldIndex(CurrentFold,:)==1));
    end
   %% Verify that the permutation did not fail
%     figure
%     plot(XtestIn,DtestOut,'.');
%     figure
%     plot(XtrainIn,DtrainOut,'.');
%     figure
%     plot(Xtrain{CurrentFold},Dtrain{CurrentFold},'.');
%     figure
%     plot(Xtest{CurrentFold},Dtest{CurrentFold},'.');
end

function [f,g] = make_fg (a,b)
%Generate transfer function and its derivative
%input
%   a : slope of the transfer function
%output
%   f : hyperbolic tangent with slope 'a'
%   g : derivative of function 'f'
    f = @(x)(a*tanh(b*x));
    g = @(x)( a*b*(1-tanh(b*x).^2));
end 